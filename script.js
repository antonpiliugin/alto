let currentPage = 1;
const perPage = 4;
const productsList = document.getElementById("products");
const previous = document.getElementById("previous");
const paginationPages = document.getElementById("pagination-pages");
const next = document.getElementById("next");

const init = () => {
  previous.addEventListener("click", () => {
    if (currentPage - 1 >= 1) {
      currentPage--;
      getData(currentPage).then((json) => renderData(json));
    }
  });
  next.addEventListener("click", () => {
    currentPage++;
    getData(currentPage).then((json) => renderData(json));
  });
};

const getData = (page = 1) => {
  const offset = page <= 1 ? null : (page - 1) * perPage;
  const offsetQuery = offset !== null ? `?skip=${offset}` : "";

  return fetch(`http://testtask.alto.codes/front-products.php${offsetQuery}`)
    .catch((error) => console.error(error))
    .then((response) => response.json());
};

const pagination = (total) => {
  if (total < 1) {
    return;
  }
  const pages = Math.ceil(total / perPage);
  const pagesMax = currentPage + 3;
  const lastPage = pagesMax > pages ? pages : pagesMax;
  const firstPage = lastPage - 3;
  const previousElipsis = firstPage > 1;
  const nextElipsis = lastPage < pages;

  if (currentPage > 1) {
    previous.disabled = false;
  } else {
    previous.disabled = true;
  }

  if (currentPage >= pages) {
    next.disabled = true;
  } else {
    next.disabled = false;
  }

  while (paginationPages.firstChild) {
    paginationPages.firstChild.remove();
  }

  if (previousElipsis) {
    const previousElipsisButton = document.createElement("button");
    previousElipsisButton.innerText = "...";
    previousElipsisButton.classList.add("pagination-page");
    previousElipsisButton.addEventListener("click", () => {
      const previousElipsisFirst = firstPage - perPage;
      const previousElipsisPage =
        previousElipsisFirst < 1 ? 1 : previousElipsisFirst;
      currentPage = previousElipsisPage;
      getData(currentPage).then((json) => renderData(json));
    });
    paginationPages.appendChild(previousElipsisButton);
  }

  for (let i = firstPage; i <= lastPage; i++) {
    const pageButton = document.createElement("button");
    pageButton.innerText = i;
    pageButton.classList.add("pagination-page");
    if (i === currentPage) {
      pageButton.classList.add("active");
    }
    pageButton.addEventListener("click", () => {
      currentPage = i;
      getData(i).then((json) => renderData(json));
    });
    paginationPages.appendChild(pageButton);
  }

  if (nextElipsis) {
    const nextElipsisButton = document.createElement("button");
    nextElipsisButton.innerText = "...";
    nextElipsisButton.classList.add("pagination-page");
    nextElipsisButton.addEventListener("click", () => {
      const nextElipsisLast = lastPage + 1;
      const nextElipsisPage = nextElipsisLast > pages ? pages : nextElipsisLast;
      currentPage = nextElipsisPage;
      getData(currentPage).then((json) => renderData(json));
    });
    paginationPages.appendChild(nextElipsisButton);
  }
};

const products = (products) => {
  while (productsList.firstChild) {
    productsList.firstChild.remove();
  }

  products.forEach((product) => {
    const productContainer = document.createElement("div");
    productContainer.classList.add("product");


    const imageContainer = document.createElement("div");
    imageContainer.classList.add("product-image-container");
    const image = document.createElement("img");
    image.classList.add("product-image");
    image.src = product.image_url;
    imageContainer.appendChild(image)
    productContainer.appendChild(imageContainer);

    const detailsContainer = document.createElement("div");
    detailsContainer.classList.add("product-details");

    const availability = document.createElement("div");
    availability.classList.add("product-availability");
    if (product.availability) {
      availability.innerText = "В наличии";
      availability.classList.add("in-stock");
    } else {
      availability.innerText = "Под заказ";
      availability.classList.add("on-order");
    }
    detailsContainer.appendChild(availability);

    const name = document.createElement("div");
    name.innerText = product.name;
    name.classList.add("product-name");
    detailsContainer.appendChild(name);

    const price = document.createElement("div");
    price.innerHTML = `${product.price} &#8381;`;
    price.classList.add("product-price");
    detailsContainer.appendChild(price);

    const color = document.createElement("div");
    color.innerText = `Цвет - ${product.color}`;
    color.classList.add("product-color");
    detailsContainer.appendChild(color);

    const description = document.createElement("div");
    description.innerText = product.short_desc;
    description.classList.add("product-description");
    detailsContainer.appendChild(description);

    if (product.availability) {
      const addToCart = document.createElement("button");
      const addToCartIcon = document.createElement("img");
      addToCartIcon.src = "cart.svg";
      addToCartIcon.classList.add("add-to-cart-icon");
      addToCart.appendChild(addToCartIcon);
      const addToCartText = document.createElement("span");
      addToCartText.innerText = "В корзину";
      addToCartText.classList.add("add-to-cart-text");
      addToCart.appendChild(addToCartText);
      addToCart.classList.add("add-to-cart");
      addToCart.addEventListener("click", () => alert(`Товар с ID ${product.id} добавлен в корзину!`));
      detailsContainer.appendChild(addToCart);
    }

    productContainer.appendChild(detailsContainer);
    productsList.appendChild(productContainer);
  });
};

const renderData = (data) => {
  products(data.products || []);
  pagination(data.totalCount || 0);
};

document.addEventListener("DOMContentLoaded", () => {
  init();
  getData().then((json) => renderData(json));
});
